#include<iostream>
#include "vacuum_cleaner.h"

using namespace std;

void main()
{
	Vacuum_cleaner bosch;
	bosch.Reset();
	do
	{
		cout << "-----Model of the vacuum cleaner-----" << endl;
		cout << "_____________________________________" << endl;
		cout << "      Choose one of the actionsz:    " << endl;
		cout << "_____________________________________" << endl;
		cout << " 1 - Connect to the mains \n 2 - Disconnect from the mains \n 3 - Turn on \n 4 - Turn off \n 5 - Clean the container \n 0 - Exit" << endl;
		cout << "_____________________________________" << endl;
		cout << "\n";
		cout << "The condition of the vacuum cleaner at the moment: " << endl;
		cout << "  Power: " << bosch.GetStateNamePower() << endl;
		cout << "  Engine: " << bosch.GetStateNameEngine() << endl;
		cout << "  Occupancy indicator: "<<bosch.GetStateNameIndicator() << endl;
		cout << "  Energy: " << bosch.Get_Energy() << endl;
		cout << "  The volume of the container: " << bosch.Get_Volume() << endl;
		cout << "-------------------------------------" << endl;
		cout << "\n";
		cout << "Enter number: ";
		int key;
		cin >> key;
		cin.get();
		switch (key)
		{
			case 1:
			{
				bosch.Connect();
			}break;
			case 2:
			{
				bosch.Disconnect();
			}break;
			case 3:
			{
				bosch.TurnOn();
			}break;
			case 4:
			{
				bosch.TurnOff();
			}break;
			case 5:
			{
				bosch.CleaningContainer();
			}break;
			case 0:
			{
				return;
			}break;
			default:
			{
				cout << "Error! Invalid operation! Press any key to continue..." << endl;
			}break;
		}
	} while (true);
}