#pragma once

const float MAXVOLUME = 2.00;
const float MINVOLUME = 0.00;

enum VacuumStatePower {CONNECT,DISSCONECT};
const char VacuumStateNamePower[2][12] = { "CONNECT", "DISCONNECT" };
enum VacuumStateEngine { OFF, ON };
const char VacuumStateNameEngine[2][4] = { "OFF", "ON" };
enum VacuumStateIndicator { ENABLED, DISABLED };
const char VacuumStateNameIndicator[2][10] = { "ENABLED", "DISABLED" };
class Vacuum_cleaner
{
private:
	bool Power;
	int StatePower;
	int StateEngine;
	int StateIndicator;
	bool Engine_Condition;
	bool Indicator_Status;
	int Energy;
	float Volume_Container;

	void SetStatePower(int);
	void SetStateEngine(int);
	void SetStateIndicator(int);
public:
	void Reset(void);

	void Connect(void);
	void Disconnect(void);
	void TurnOn(void);
	void TurnOff(void);
	void Set_Energy(int);
	int Get_Energy(void) { return Energy; }
	bool Get_Power(void) { return Power; }
	bool Get_Indicator_Status(void) { return Indicator_Status; }
	bool Get_Engine_Condition(void) { return Engine_Condition; }
	float Get_Volume(void) { return Volume_Container; }
	float FillingContainer(float);
	float CleaningContainer(void);
	int GetStatePower(void);
	int GetStateEngine(void);
	int GetStateIndicator(void);
	const char* const GetStateNamePower() { return VacuumStateNamePower[StatePower]; }
	const char* const GetStateNameEngine() { return VacuumStateNameEngine[StateEngine]; }
	const char* const GetStateNameIndicator() { return VacuumStateNameIndicator[StateIndicator]; }
};

