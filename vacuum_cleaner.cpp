#include<iostream>
#include "vacuum_cleaner.h"

using namespace std;

void Vacuum_cleaner::Reset(void)
{
	Power = false;
	SetStatePower(DISSCONECT);
	Engine_Condition = false;
	SetStateEngine(OFF);
	Indicator_Status = false;
	SetStateIndicator(DISABLED);
	Energy = 1000;
	Volume_Container = 0.0;
}
void Vacuum_cleaner::SetStatePower(int NewState)
{
	StatePower = NewState;
}
void Vacuum_cleaner::SetStateEngine(int NewState)
{
	StateEngine = NewState;
}
void Vacuum_cleaner::SetStateIndicator(int NewState)
{
	StateIndicator = NewState;
}
void Vacuum_cleaner::Connect(void)
{
	if (!Power)
	{
		Power = true;
		SetStatePower(CONNECT);
	}
}

void Vacuum_cleaner::Disconnect(void)
{
	if (Power)
	{
		Power = false;
		SetStatePower(DISSCONECT);
		Engine_Condition = false;
		SetStateEngine(OFF);
		Indicator_Status = false;
		SetStateIndicator(DISABLED);
	}
}

void Vacuum_cleaner::Set_Energy(int en)
{
	Energy = en;
}

void Vacuum_cleaner::TurnOn(void)
{
	if (Volume_Container == 2)
	{
		cout << "\n\n";
		cout << "The container is full !!!" << endl;
		cout << "\n";
	}
	else
	{
		if (!Engine_Condition)
		{
			if (Power)
			{
				Engine_Condition = true;
				SetStateEngine(ON);
				do
				{
					cout << "\n *Set the power value: ";
					int energy;
					cin >> energy;
					if (energy > 999 && energy < 1501)
					{
						Set_Energy(energy);
						break;
					}
					else
					{
						cout << "You have set the wrong value !" << endl;
					}
				} while (true);
				if (Volume_Container > 1.83)
				{
					Indicator_Status = true;
					SetStateIndicator(ENABLED);
				}
			}

		}
	}
}

void Vacuum_cleaner::TurnOff(void)
{
	if (Engine_Condition)
	{
		Engine_Condition = false;
		SetStateEngine(OFF);
		Indicator_Status = false;
		SetStateIndicator(DISABLED);

		float Area;
		cout << "\nEnter the vacuumed area: ";
		cin >> Area;
		cin.get();
		float Coefficient;
		cout << "\nEnter the dust ratio: ";
		cin >> Coefficient;
		cin.get();
		float Volume = Area * Coefficient;
		FillingContainer(Volume);
	}
}

float Vacuum_cleaner::FillingContainer(float Volume)
{ 
	if (Volume < 0)
	{
		return 0;
	}
	else
	{
		float NewVolume = Volume_Container + Volume;
		if (NewVolume > MAXVOLUME)
		{
			Volume_Container = MAXVOLUME;
			return MAXVOLUME - Volume;
		}
		else
		{
			Volume_Container = NewVolume;
			return Volume;
		}
	}
	
}

float Vacuum_cleaner::CleaningContainer()
{
	if (Volume_Container < 0.01)
	{
		cout << "\n The container is clean.\n" << endl;
	}
	else
	{
		Volume_Container = 0.0;
		cout << "\n";
		cout << "\n The container is clean. \n";
		cout << "\n";
		return Volume_Container;
	}
}